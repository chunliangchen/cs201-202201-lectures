package lectures.selection;

import java.util.Scanner;

public class ExampleThirteen {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in); // create scanner to get user input
		
		System.out.print("Enter a 'Hello': ");
		String value = input.nextLine();
		
		input.close();
		
		String str1 = "Hello";
		String str2 = str1;
		
		System.out.println(str1 == str2); // same location in memory
		System.out.println(str1 == value); // might have the same value, but not the same memory location
		System.out.println(str1.equals(value)); // same value
		System.out.println(str1.equalsIgnoreCase(value)); // same value (ignoring case)
	}

}
