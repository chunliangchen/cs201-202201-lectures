package lectures.selection;

import java.util.Scanner;

public class ExampleTen {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		// prompt for code
		System.out.print("Code: ");
		int i = Integer.parseInt(input.nextLine());
		
		// print out character
		System.out.println("Letter: " + ((char)i));
		
		input.close();
	}

}
