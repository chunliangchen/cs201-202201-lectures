package lectures.selection;

import java.util.Scanner;

public class ExampleSix {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		// prompt the user for a letter
		System.out.print("Letter: ");
		
		// read in a character
		char c = input.nextLine().charAt(0);
		
		// pick options from user input
		if (c == 'a') {
			System.out.println("apple");
		} else if (c == 'b') {
			System.out.println("bat");
		} else if (c == 'c') {
			System.out.println("cat");
		} else {
			System.out.println("not a, b, or c");
		}
	}

}
