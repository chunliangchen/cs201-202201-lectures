package lectures.selection;

import java.util.Scanner;

public class ExampleEleven {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner for user input
		
		System.out.print("Enter a, b, or c: ");
		char c = input.nextLine().charAt(0);
		
		switch (c) {
			case 'a':
			case 'A':
				System.out.println("Apple");
				break;
			case 'b':
			case 'B':
				System.out.println("Bat");
				break;
			case 'c':
			case 'C':
				System.out.println("Cat");
				break;
			default:
				System.out.println("Not a, b, or c");
		}
		
		input.close();
	}

}
