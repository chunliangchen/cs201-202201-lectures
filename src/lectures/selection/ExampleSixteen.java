package lectures.selection;

public class ExampleSixteen {

	public static void main(String[] args) {
		String str1 = "apple";
		String str2 = "a";
		
		System.out.println(str1.equals(str2));
		System.out.println(str1.compareTo(str2));
	}

}
