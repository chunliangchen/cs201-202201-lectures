package lectures.selection;

import java.util.Scanner;

public class ExampleFive {

	public static void main(String[] args) {
		// create a Scanner for user input
		Scanner input = new Scanner(System.in);
		
		// prompt the user
		System.out.print("X=");
		double x = Double.parseDouble(input.nextLine()); // Read in String and convert to double
		
		// prompt the user again
		System.out.print("Y=");
		double y = Double.parseDouble(input.nextLine()); // Read in another String and convert to double
		
		System.out.println("x + y = " + (x+y));
	}

}
