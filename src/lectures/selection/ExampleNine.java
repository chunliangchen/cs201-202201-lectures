package lectures.selection;

import java.util.Scanner;

public class ExampleNine {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		// prompt for letter
		System.out.print("Letter: ");
		char c = input.nextLine().charAt(0);
		
		// print out character code
		System.out.println("Code: " + ((int)c));
		
		input.close();
	}

}
