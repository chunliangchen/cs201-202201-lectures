package lectures.intro;

public class ExampleOne {

	public static void main(String[] args) {
		int x = 9; // create a variable x and assign a value
		int y = 42; // create a variable y and assign a value
		
		// print out values of variables
		System.out.println(x);
		System.out.println(y);
	}

}
